#This Python script is made for the download graphics feature on the investigation pages.
#It may require running '$ pip install altair_saver' to output in the form of an image.
#!/usr/bin/env python3

import json
from flask import Flask
from flask import send_file
from flask import request
import altair as alt

# Create an app
app = Flask(__name__)

#This returns the name picture file created in the current folder.
@app.route('/get_graphic_as_picture/<string:desiredFormat>', methods=['GET', 'POST'])
def get_image(desiredFormat):
	jsonGraphic = request.json
	chart = alt.Chart.from_dict(jsonGraphic)

	if desiredFormat in ['png', 'svg', 'pdf'] :
		chart.save('tempGraphic.' + desiredFormat)
	
	savedFileName = 'tempGraphic.' + desiredFormat

	return send_file(savedFileName)

app.run(host='0.0.0.0')
