/* eslint-disable no-useless-catch */
import elasticsearch from 'elasticsearch';

var client = new elasticsearch.Client({
  host: process.env.VUE_APP_DB,
  log: 'info'
});

export const elasticSearchMixin = {
  methods: {
    checkServer: function () {
      client.ping({
        // ping usually has a 3000ms timeout
        requestTimeout: 1000
      }, function (error) {
        if (error) {
          console.trace('elasticsearch cluster is down!');
        }
      });
    },
    /**
     * Get stats of an index.
     * @param {*} index
     */
    getIndexStats: function (index) {
      return client.indices.stats({
        index: index
      });
    },
    /**
     * Get stats of an index.
     * e.g number of documents in an index.
     * @param {*} index
     */
    getDocumentCount: function (index) {
      return client.count({
        index: index
      });
    },
    /**
     * [
        { "match_phrase": { "topic": "Education" } },
        { "match_phrase": { "topic": "Employment" } }
      ]
     * @param {*} field
     * @param {*} fieldDataArray
     */
    buildMatchPhraseArray: function (field, fieldDataArray) {
      var queryArray = [];
      if (fieldDataArray === undefined || fieldDataArray === '' || fieldDataArray.length === 0) {
        queryArray = [];
      } else if (typeof (fieldDataArray) === 'string') {
        queryArray.push({ match_phrase: { [field]: fieldDataArray } }); // incase it is only one search term.
      } else {
        fieldDataArray.forEach((fieldData) => {
          queryArray.push({ match_phrase: { [field]: fieldData } });
        });
      }
      return queryArray;
    },
    buildSubBoolArray: function (field, fieldDataArray) {
      var queryArray = [];
      if (fieldDataArray === undefined || fieldDataArray === '' || fieldDataArray.length === 0) {
        queryArray = [];
      } else if (typeof (fieldDataArray) === 'string') {
        queryArray.push({ match_phrase: { [field]: fieldDataArray.toLowerCase() } }); // incase it is only one search term.
      } else {
        fieldDataArray.forEach((fieldData) => {
          queryArray.push({ match_phrase: { [field]: fieldData.toLowerCase() } });
        });
      }
      var query = {
        bool: {
          should: [
            ...queryArray
          ]
        }
      };
      return query;
    },
    buildNestedSearch: function (parentPath, field, fieldDataArray) {
      var queryArray = [];
      if (fieldDataArray === undefined || fieldDataArray === '') {
        queryArray = [];
      } else if (typeof (fieldDataArray) === 'string') {
        queryArray.push({ match_phrase: { [field]: fieldDataArray } }); // incase it is only one search term.
      } else {
        fieldDataArray.forEach((fieldData) => {
          queryArray.push({ match_phrase: { [field]: fieldData } });
        });
      }
      var query = [
        {
          nested: {
            path: parentPath,
            query: {
              bool: {
                must: [
                  ...queryArray
                ]
              }
            }
          }
        }
      ];
      return query;
    },
    buildTermArray: function (topics, locations) {
      var termArray = [];
      topics.forEach((topic) => {
        termArray.push({ term: { topics: topic.key.toLowerCase() } });
      });
      locations.forEach((location) => {
        termArray.push({ term: { locations: location.key } });
      });
      return termArray;
    },
    /**
     * Search query for
     *
     * @param {*} topics
     * @param {*} sources
     * @param {*} organizations
     */
    queryTopicResults: function (cats, locs, text, sources, organizations, types, natures, locales, startFromID = 1, resultsPerPage = 10) {
      var catsQuery = this.buildSubBoolArray('topics', cats);
      var locsQuery = this.buildSubBoolArray('locs', locs);
      var sourceQuery = this.buildMatchPhraseArray('source_name', sources);
      var organizationMatchPhraseQuery = this.buildNestedSearch('result.organization', 'result.organization.title', organizations);
      var typesQuery = [];
      if (types.length) {
        typesQuery.push([
          {
            exists: {
              field: 'types'
            }
          }
        ]);
      }
      var localesQuery = [];
      if (locales.length) {
        localesQuery = [
          {
            bool: {
              should: locales.map(function (locale) {
                return {
                  prefix: {
                    locale: locale
                  }
                };
              })
            }
          }
        ];
      }
      var naturesQuery = this.buildMatchPhraseArray('nature', natures);
      var textQuery = [];
      if (text) {
        textQuery.push(
          {
            nested: {
              path: 'result',
              score_mode: 'avg',
              query: {
                bool: {
                  must: [
                    {
                      multi_match: {
                        query: text,
                        fields: [
                          'result.name',
                          'result.notes',
                          'result.id',
                          'result.title.id',
                          'result.organization.description',
                          'result.organization.title',
                          'result.organization.name'
                        ],
                        operator: 'or',
                        type: 'phrase'
                      }
                    }
                  ]
                }
              }
            }
          }
        );
      }
      var currentDate = new Date();
      /**
       * Additionally to querying, this method also submits the query terms to a secondary index to assist the mostUsedTopics methods.
       */
      client.index({
        index: process.env.VUE_APP_DB_INDEX_2,
        type: '_doc',
        body: {
          date: currentDate,
          query: cats
        }
      }
      );
      return client.search({
        index: process.env.VUE_APP_DB_INDEX,
        type: '_doc',
        from: startFromID,
        size: resultsPerPage,
        body: {
          sort: [{
            'result.resources.created': {
              order: this.sortBy,
              mode: 'max',
              nested: {
                path: 'result.resources'
              }
            }
          }],
          query: {
            bool: {
              must: [
                catsQuery,
                locsQuery,
                ...sourceQuery,
                ...organizationMatchPhraseQuery,
                ...typesQuery,
                ...naturesQuery,
                ...localesQuery,
                ...textQuery
              ]
            }
          }
        }
      });
    },
    queryTopicResultsByScore: function (cats, locs, text, sources, organizations, types, natures, locales, startFromID = 1, resultsPerPage = 10) {
      var catsQuery = this.buildSubBoolArray('topics', cats);
      var locsQuery = this.buildSubBoolArray('locs', locs);
      var sourceQuery = this.buildMatchPhraseArray('source_name', sources);
      var organizationMatchPhraseQuery = this.buildNestedSearch('result.organization', 'result.organization.title', organizations);
      var typesQuery = [];
      if (types.length) {
        typesQuery.push([
          {
            exists: {
              field: 'types'
            }
          }
        ]);
      }
      var localesQuery = [];
      if (locales.length) {
        localesQuery = [
          {
            bool: {
              should: locales.map(function (locale) {
                return {
                  prefix: {
                    locale: locale
                  }
                };
              })
            }
          }
        ];
      }
      var naturesQuery = this.buildMatchPhraseArray('nature', natures);
      var textQuery = [];
      if (text) {
        textQuery.push(
          {
            nested: {
              path: 'result',
              score_mode: 'avg',
              query: {
                bool: {
                  must: [
                    {
                      multi_match: {
                        query: text,
                        fields: [
                          'result.name',
                          'result.notes',
                          'result.id',
                          'result.title.id',
                          'result.organization.description',
                          'result.organization.title',
                          'result.organization.name'
                        ],
                        operator: 'or',
                        type: 'phrase'
                      }
                    }
                  ]
                }
              }
            }
          }
        );
      }
      var currentDate = new Date();
      /**
       * Additionally to querying, this method also submits the query terms to a secondary index to assist the mostUsedTopics methods.
       */
      client.index({
        index: process.env.VUE_APP_DB_INDEX_2,
        type: '_doc',
        body: {
          date: currentDate,
          query: cats
        }
      }
      );
      return client.search({
        index: process.env.VUE_APP_DB_INDEX,
        type: '_doc',
        from: startFromID,
        size: resultsPerPage,
        body: {
          sort: [{
            _score: {
              order: this.relevance
            }
          }],
          query: {
            bool: {
              must: [
                catsQuery,
                locsQuery,
                ...sourceQuery,
                ...organizationMatchPhraseQuery,
                ...typesQuery,
                ...naturesQuery,
                ...localesQuery,
                ...textQuery
              ]
            }
          }
        }
      });
    },
    uniqueListForField: function (field) {
      return client.search({
        index: process.env.VUE_APP_DB_INDEX,
        type: '_doc',
        body: {
          aggs: {
            unique_field: {
              terms: {
                field: field, // keyword is added so we do not have to enable fielddata
                size: 10000 // the number of aggreated to return. defaults to 10.
              }
            }
          }
        }
      });
    },
    getUniqueFilters: function (cats, locs, field, parentPath) {
      var catsMatchPhraseArray = [this.buildSubBoolArray('topics', cats)];
      var locsMatchPhraseArray = [this.buildSubBoolArray('locs', locs)];
      var result = '';
      if (parentPath) {
        result = client.search({
          index: process.env.VUE_APP_DB_INDEX,
          type: '_doc',
          body: {
            query: {
              bool: {
                should: [
                  ...catsMatchPhraseArray,
                  ...locsMatchPhraseArray
                ]
              }
            },
            aggs: {
              unique_field: {
                nested: {
                  path: parentPath
                },
                aggs: {
                  name: {
                    terms: {
                      field: field // + ".keyword" // keyword is added so we do not have to enable fielddata
                    }
                  }
                }
              }
            }
          }
        });
      } else {
        result = client.search({
          index: process.env.VUE_APP_DB_INDEX,
          type: '_doc',
          body: {
            query: {
              bool: {
                should: [
                  ...catsMatchPhraseArray,
                  ...locsMatchPhraseArray
                ]
              }
            },
            aggs: {
              name: {
                terms: {
                  field: field // + ".keyword" // keyword is added so we do not have to enable fielddata
                }
              }
            }
          }
        });
      }
      console.log('getting organization');
      return result;
    },
    mostUsedKeywords: function () {
      var minTimeRange = 'now-30d/d';
      var maxTimeRange = 'now/d';

      return client.search({
        index: 'queries',
        type: '_doc',
        body: {
          query: {
            bool: {
              filter: {
                range: {
                  date: {
                    gt: minTimeRange,
                    lte: maxTimeRange
                  }
                }
              },
              must: {
                match_all: {}
              }
            }
          },
          aggs: {
            keywords: {
              terms: {
                field: 'query'
              }
            }
          }
        }
      });
    },
    getDocument: function (id) {
      return client.get({
        index: 'core',
        type: '_doc',
        id: id
      });
    },
    getCkanPackages: function (ids, startFromID = 1, resultsPerPage = 10) {
      return client.search({
        index: process.env.VUE_APP_DB_INDEX,
        type: '_doc',
        size: resultsPerPage,
        body: {
          query: {
            terms: {
              lintol_package_id: ids
            }
          }
        }
      });
    },
    checkTopicsExist: function (topics, locations) {
      var termArray = this.buildTermArray(topics, locations);
      return client.search({
        index: process.env.VUE_APP_DB_INDEX,
        type: '_doc',
        body: {
          query: {
            bool: {
              should: [
                // { term : { topics : "hospitals" } },
                ...termArray
              ]
            }
          }
        },
        filter_path: 'hits.total'
      });
    }
  }
};
