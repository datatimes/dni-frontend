/* eslint-disable no-useless-catch */
export const FILTER_SOURCES = 'filterSources';
export const FILTER_ORGANIZATIONS = 'filterOrganizations';
export const FILTER_TYPES = 'filterTypes';
export const FILTER_LOCALES = 'filterLocales';
export const FILTER_NATURES = 'filterNatures';
export const SET_LOGGED_IN_USER = 'SET_LOGGED_IN_USER';
