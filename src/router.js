/* eslint-disable no-useless-catch */
import Vue from 'vue';
import Router from 'vue-router';
import SearchPage from './views/SearchPage.vue';
import DataSetPage from './views/DataSetPage.vue';
import InvestigationPage from './views/InvestigationPage.vue';
import SearchResults from './views/SearchResults.vue';
import Favourites from './views/Favourites.vue';
import About from './views/About.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'searchpage',
      component: SearchPage
    },
    {
      path: '/searchresults',
      name: 'searchresults',
      component: SearchResults,
      // results/?tags=education&tags=employment
      props: (route) => {
        var cats = route.query.cats;
        var locs = route.query.locs;
        var text = route.query.text;

        if (cats && typeof (cats) === 'string') {
          cats = cats.split('|');
        } else if (!Array.isArray(cats)) {
          cats = [];
        }

        if (locs && typeof (locs) === 'string') {
          locs = locs.split('|');
        } else if (!Array.isArray(locs)) {
          locs = [];
        }

        if (!text || typeof (text) !== 'string') {
          text = undefined;
        }

        return {
          cats: cats,
          locs: locs,
          text: text
        };
      }
    },
    {
      path: '/favourites',
      name: 'favourites',
      component: Favourites,
      // results/?tags=education&tags=employment
      props: (route) => ({
        favourites: (route.query.favourites = true)
      })
    },
    {
      path: '/dataset/:id',
      name: 'dataset',
      component: DataSetPage,
      props: true
    },
    {
      path: '/about',
      name: 'about',
      component: About
    },
    {
      path: '/ip/:id',
      name: 'investigation',
      component: InvestigationPage,
      props: true
    }
  ]
});
