/* eslint-disable no-useless-catch */
/**
 * Sets up a blank array if the key does not exist in local
 * storage.
 * @param {*} key
 */
export function setupStorage (key) {
  if (!localStorage.getItem(key)) {
    localStorage.setItem(key, JSON.stringify([]));
  }
}

/**
 * If a value is in a key that is an array in localstorage.
 *
 * @param {*} key
 * @param {*} value
 */
export function exists (key, value) {
  var exists = false;
  if (localStorage.getItem(key)) {
    var items = JSON.parse(localStorage.getItem(key));
    if (items.indexOf(value) !== -1) {
      exists = true;
    } else {
      exists = false;
    }
  } else {
    exists = false;
  }
  return exists;
}

/**
 * Add a value to the key in localstorage.
 * @param {*} key
 * @param {*} value
 */
export function addValueToKey (key, value) {
  var items = JSON.parse(localStorage.getItem(key));
  items.push(value);
  localStorage.setItem(key, JSON.stringify(items));
}

/**
 * Remove value from key in localstorage
 * @param {*} key
 * @param {*} value
 */
export function removeValueFromKey (key, value) {
  var items = JSON.parse(localStorage.getItem(key));
  var index = items.indexOf(value);
  if (index !== -1) {
    items.splice(index, 1);
  }
  localStorage.setItem(key, JSON.stringify(items));
}

/**
 * Get the array from the localstorage based on the key
 * @param {*} key
 */
export function getArrayValue (key) {
  var array = [];
  if (localStorage.getItem(key)) {
    array = JSON.parse(localStorage.getItem(key));
  }
  return array;
}

/**
 * Get the array from the localstorage based on the key
 * @param {*} key
 */
export function isStoragePopulated (key) {
  var result = false;
  if (localStorage.getItem(key)) {
    var items = JSON.parse(localStorage.getItem(key));
    if (items.length > 0) {
      result = true;
    } else {
      result = false;
    }
  } else {
    result = false;
  }
  return result;
}
