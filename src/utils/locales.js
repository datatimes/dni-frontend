/* eslint-disable no-useless-catch */
import LocaleCode from 'locale-code';
import ISO6391 from 'iso-639-1';

export function getLocaleName (locale) {
  var code = locale.replace('_', '-');
  var value = '';

  if (ISO6391.validate(code)) {
    value = ISO6391.getName(code) + ' (unknown)';
  } else if (LocaleCode.validate(code)) {
    value = LocaleCode.getLanguageName(code) + ' (' + LocaleCode.getCountryName(code) + ')';
  }
  return value;
}
