/* eslint-disable no-useless-catch */
import Vue from 'vue';
import Vuex from 'vuex';
import VuePapaParse from 'vue-papa-parse';

Vue.use(VuePapaParse);
Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    feed: {
      name: 'FOI Feed',
      url: '',
      limit: 5
    },
    host: process.env.VUE_APP_LARAVEL_HOST || '',
    newsData: [],
    dataInfo: {},
    graphics: undefined,
    investigationPage: undefined,
    availableInvestigationPages: {},
    parsedDataset: {}

  },
  mutations: {
    setInvestigationPage (state, data) {
      state.investigationPage = data;
    },

    setAvailableInvestigationPages (state, data) {
      state.availableInvestigationPages = data;
    },

    setParsedDataset (state, data) {
      state.parsedDataset = data;
    },

    getGraphics (state, data) {
      state.graphics = data;
    },

    getElasticSearchData (state, data) {
      state.dataInfo = data;
    },

    getEsGraphicsErrorCodes (state, data) {
      state.esErrorCodes = data;
    },

    getFeedUrl (state, data) {
      state.feed.url = data;
    },

    getGraphicsFromEs (state, data) {
      state.graphics = data;
    },

    getNewsData (state, data) {
      state.newsData = data;
    }
  },
  actions: {
    loadInvestigationPage (_, id) {
      try {
        // TODO: Create the other Lintol Processed Documents for the others subjects.
        fetch('/api/ip/'.concat(id))
          .then(response => response.text())
          .then(text => JSON.parse(text))
          .then(data => this.commit('setInvestigationPage', data));
      } catch (error) {
        console.log(error);
      }
    },

    getGraphics ({ state }, id) {
      var graphicsIds = state.investigationPage.graphics;
      console.log(graphicsIds);
      var graphicsData = [];
      var i;

      for (i = 0; i < graphicsIds.length; i++) {
        try {
          fetch('/ip/' + id + '/graphics/' + graphicsIds[i])
            .then(response => response.text())
            .then(text => JSON.parse(text))
            .then(data => graphicsData.push(data));
        } catch (error) {
          console.log(error);
        }
      }

      this.commit('getGraphics', graphicsData);
    },

    getAvailableInvestigationPages () {
      try {
        // TODO: Create the other Lintol Processed Documents for the others subjects.
        fetch('/api/get_investigation_pages')
          .then(response => response.text())
          .then(text => JSON.parse(text))
          .then(data => this.commit('setAvailableInvestigationPages', data));
      } catch (error) {
        console.log(error);
      }
    },

    /* getElasticSearchData({ commit }){
      try {
        //TODO: Create the other Lintol Processed Documents for the others subjects.
        fetch(('http://127.0.0.1:5000/elastic_search_data/').concat('_Z2zQW0BdeI7J_hL9rAw'))
          .then(response => response.text())
          .then(text => JSON.parse(text))
          .then(data => this.commit('getElasticSearchData', data));
      } catch (error) {
      console.log(error);
      }
    }, */

    /*
    getFeedUrl({ commit }) {
      try {
        fetch(('http://127.0.0.1:5000/foifeed_url/').concat(this.state.InvestigationPageID))
          .then(response => response.text())
          .then(data => this.commit('getFeedUrl', data));
      } catch (error) {
      console.log(error);
      }
    }, */

    /*
    getGraphicsFromEs({commit}) {
      try {
        fetch(('http://127.0.0.1:5000/get_graphics_from_es/').concat(this.state.InvestigationPageID))
          .then(response => response.text())
          .then(text => JSON.parse(text))
          .then(data => this.commit('getGraphicsFromEs', data));
      } catch (error) {
      console.log(error);
      }
    }, */

    /*
    getNewsData({commit}) {
      try {
        fetch(('http://127.0.0.1:5000/news_data/').concat(this.state.InvestigationPageID))
          .then(response => response.text())
          .then(text => JSON.parse(text))
          .then(data => this.commit('getNewsData', data));
      } catch (error) {
        console.log(error);
        }
    }, */

    parseDataset ({ commit }, url) {
      this._vm.$papa.parse(url, {
        download: true,
        complete: function (results) {
          commit('setParsedDataset', results.data);
        }
      });
    },

    setInvestigationPageID (_, id) {
      this.commit('setInvestigationPageID', id);
    }
  }
});
