/* eslint-disable no-useless-catch */
import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import BootstrapVue from 'bootstrap-vue';
import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';
import VueWait from 'vue-wait';
import { library } from '@fortawesome/fontawesome-svg-core';
import { faBars, faSearch, faChevronRight, faChevronLeft, faStar, faQuestionCircle } from '@fortawesome/free-solid-svg-icons';
import {
  faTwitter,
  faFacebook,
  faStackOverflow,
  faGithub
} from '@fortawesome/free-brands-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import VoerroTagsInput from '@voerro/vue-tagsinput';
import VueEvents from 'vue-events';
import elasticsearch from 'elasticsearch';
import moment from 'moment';
import VueCarousel from 'vue-carousel';
import lineClamp from 'vue-line-clamp';
import VTooltip from 'v-tooltip';
import { HotTable } from '@handsontable/vue';

library.add(faBars, faSearch, faChevronLeft, faChevronRight, faTwitter, faFacebook, faStackOverflow, faGithub, faStar, faQuestionCircle);
Vue.component('font-awesome-icon', FontAwesomeIcon);
Vue.component('hot-table', HotTable);

// import { library } from '@fortawesome/fontawesome-svg-core';
// import { faUser, faSpinner } from '@fortawesome/free-solid-svg-icons';
// import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';

// library.add(faUser);
// library.add(faSpinner);

// Vue.component('font-awesome-icon', FontAwesomeIcon);

Vue.filter('convertCkanTime', function (time) {
  return moment(time).format('Do MMMM YYYY');
});

Vue.use(VueWait);
Vue.use(lineClamp);
Vue.use(VueEvents);
Vue.use(BootstrapVue);
Vue.use(VueCarousel);
Vue.use(VTooltip);

Vue.component('tags-input', VoerroTagsInput);

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  render: h => h(App),
  wait: new VueWait(),
  data: {
    show: true
  }
}).$mount('#app');
