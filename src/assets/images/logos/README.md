Please note that any intellectual property, (in the form of trademark
copyright or otherwise), held by third-parties in this directory
is not covered by any open source licensing or clauses that apply
to other works in this package, and remains the exclusive property of
the intellectual property owner.
